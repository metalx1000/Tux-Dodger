extends CharacterBody2D


@export var gravity = 900.0
@export var flap_force: int = -300
@onready var sprite = $Sprite
@onready var display_size = get_viewport().get_visible_rect().size

var dead = false
var max_speed = 400
var rotation_speed = 2
var score = 0
var current_score = 0

func _ready():
	Global.player = self
	velocity = Vector2.ZERO
	

func _physics_process(delta):
	if dead:
		return
		
	velocity.y += gravity * delta
	
	if velocity.y > max_speed:
		velocity.y = max_speed
		
	if position.y > display_size.y + 128 || position.y < -128:
		death()
	
	move_and_collide(velocity * delta)
	rotate_bird()
	
func _input(event):
	if event.is_pressed():
		flap()
		
func flap():
	sprite.play("fly")
	$flap.stop()
	$flap.play()
	velocity.y = flap_force

func rotate_bird():
	# downwards  
	if velocity.y > 0 and rad_to_deg(rotation) < 90:
		sprite.play("fall")
		rotation += rotation_speed * deg_to_rad(1)
	# upwards 
	elif velocity.y < 0 and rad_to_deg(rotation) > -30:
		sprite.play("default")
		rotation -= rotation_speed * deg_to_rad(1) * 2


func death():
	Global.scores.append(current_score)
	dead = true
	$death.play()
	sprite.stop()
	gravity = 0
	velocity = Vector2.ZERO
