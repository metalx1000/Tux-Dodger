extends Label

@onready var player = get_tree().get_nodes_in_group("players")[0]

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func update_score():
	if player.score < player.current_score:
		player.score += 10
		$sound.play(0)
		
	text = "Score: " + str(player.score)
