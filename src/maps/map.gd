extends Node2D

@export var pipes = preload("res://objects/pipes/pipe.tscn")

@onready var display_size = get_viewport().get_visible_rect().size
var game_over = false
var allow_restart = false

var songs = [
	"res://music/airship_2.ogg",
	"res://music/arctic_breeze.ogg",
	"res://music/chipdisko.ogg",
	"res://music/jewels.ogg"
]

func _ready():
	$black.visible = false
	Global.game_time
	randomize()
	songs.shuffle()
	$music.stream = load(songs[0])
	$music.play()
	$music.volume_db = -10
	Global.pipe_speed = 200.0
	Global.game_time = 0.0
	


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	Global.game_time += delta
	player_dead()

func player_dead():
	if !$player.dead:
		return
		
	if !game_over:
		game_over = true
		
		$black.visible = true
		$AnimationPlayer.play("death")

func enable_restart():
	allow_restart = true
	
func create_pipe():
	var pipe = pipes.instantiate()
	
	pipe.position.x = display_size.x + 128
	pipe.position.y += randi_range(-200,200)
	
	#pipe.top_pos += randi_range(-100,100) 
	pipe.top_pos += Global.game_time 
	if $PipeTimer.wait_time > 1.5:
		$PipeTimer.wait_time -= .1
	
	get_tree().current_scene.add_child(pipe)
	
func _input(event):
	if event is InputEventScreenTouch:
		$exit.visible = true

	if allow_restart:
		if event is InputEventScreenTouch || event is InputEventKey || event is InputEventMouseButton:
			get_tree().change_scene_to_file("res://scenes/title_menu.tscn")
			
func _on_exit_pressed():
	get_tree().quit()

