extends Node

var pipe_speed = 200.0
var player 
var game_time = 0.0
var scores = []

var save_file = "user://scores.save"

func _ready():
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	load_score()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pipe_speed += delta
	if Input.is_action_just_pressed("exit"):
		get_tree().quit()

func save_score():
	var file = FileAccess.open(save_file,FileAccess.WRITE)
	for score in scores:
		file.store_string(str(score)+"\n")

	file = null

func load_score():
	if !FileAccess.file_exists(save_file):
		return
	scores.clear()
	var file = FileAccess.open(save_file,FileAccess.READ)
	
	var content = file.get_as_text()
	for s in content.split("\n"):
		scores.append(int(s))
	scores.sort()
	scores.reverse()
	return content
