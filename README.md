# Tux-Dodger

Copyright Kris Occhipinti 2023-08-07

(https://filmsbykris.com)

License GPLv3

# Game Home Page
https://filmsbykris.com/games/2023/tux_dodger/

# Credits
Game Code By Kris Occhipinti

Game Assests from the Super Tux Project

https://www.supertux.org/
