#!/bin/bash
###################################################################### 
#Copyright (C) 2023  Kris Occhipinti
#https://filmsbykris.com

#This program is free software: you can redistribute it and/or modify
#it under the terms of the GNU General Public License as published by
#the Free Software Foundation version 3 of the License.

#This program is distributed in the hope that it will be useful,
#but WITHOUT ANY WARRANTY; without even the implied warranty of
#MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#GNU General Public License for more details.

#You should have received a copy of the GNU General Public License
#along with this program.  If not, see <http://www.gnu.org/licenses/>.
###################################################################### 
game="tux_dodger"
cd bin || exit 1
cp -v windows/*.pck .
zip -r ${game}_linux.zip linux && rm linux/*
zip -r ${game}_windows.zip windows && rm windows/*
zip -r ${game}_web.zip web && rm web/*

